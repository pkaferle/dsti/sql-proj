Some interesting SQL queries, including:
- connection with Python to SQL DB and data extraction
- use of cursors anf triggers to traverse the table
- creating and runnung stored procedures
- all the customers that bought all the products in the DB (division query)
- average invoice per customer per year
- how many items of each products was ordered pre year
- data consistency between orders and invoices
- ...

